package com.proximizer.client;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	public static final String TAG = "LoginActivity";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	private UserRegisterTask mRegiTask = null ;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_ACTION_DONE) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
		
		findViewById(R.id.register_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptRegister();
					}
				});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

	/**register
	 * 
	 */
	
	public void attemptRegister() {
		if (mRegiTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}/* else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}*/

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mRegiTask = new UserRegisterTask();
			mRegiTask.execute((Void) null);
		}
	}


	
	
	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}/* else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}*/

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.
			String status ="unknown" ;
			try {
			
			//	isAuthenticated=RemoteServices.authenticate(mEmail, mPassword) ;
				
				 final String  METHOD_NAME ="authenticate" ; 
				
				SoapObject request = new SoapObject(com.proximizer.client.Global.NAMESPACE, METHOD_NAME);
				 request.addProperty("arg0",mEmail);
		         request.addProperty("arg1",mPassword);
		         SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		         envelope.setOutputSoapObject(request);
		        
		         // Make the soap call.
		         HttpTransportSE androidHttpTransport = new HttpTransportSE(com.proximizer.client.Global.URL);
		         androidHttpTransport.debug=true ;
		         try {

		             //this is the actual part that will call the webservice
		             androidHttpTransport.call(com.proximizer.client.Global.SOAP_ACTION, envelope);        
		         } catch (Exception e) {
		             e.printStackTrace();
		             status ="network_error";
		         }
		         finally
		         {
		        	 String inputRequest =androidHttpTransport.requestDump ;
		             //Log.d(TAG,"Input XML request "+inputRequest);
		             
		             String outputResponse =androidHttpTransport.responseDump ;
		             //Log.d(TAG,"Output XML response "+outputResponse);
		         }
		         
		      // Get the SoapResult from the envelope body.       
		         SoapObject result = (SoapObject)envelope.bodyIn;
		         //Log.d("Soap response","result  is ....."+result);
		         
		         if (result !=null)
		         {
		        	 String authresult =result.getProperty(0).toString();
		        	 if (authresult.equals("true"))
		        			 status="pass";
		        	 else
		        		 status="fail";
		        	 
		         }
		         
		         
				return status ;
				
			} catch (Exception e) {
				return "error";
			}

		}

		@Override
		protected void onPostExecute(final String status) {
			mAuthTask = null;
			showProgress(false);

			if (status.equalsIgnoreCase("pass")) {
				//finish();
				//Start the ContactsActivity
				Intent intent = new Intent(LoginActivity.this,ContactsActivity.class);
				intent.putExtra("LoggedUser",mEmail );
				startActivity(intent);
			}
				else if(status.equalsIgnoreCase("network_error"))
				{
					AlertDialog alertDialog = new AlertDialog.Builder(
							LoginActivity.this).create();
					alertDialog.setTitle("Problem");

					alertDialog.setMessage("Looks like there is a connectivity issue !!");

					alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// here you can add functions
						}
					});
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.show();
				}
			 else if(status.equalsIgnoreCase("fail")) {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.
			boolean isRegister =false ;
			try {
			
			//	isAuthenticated=RemoteServices.authenticate(mEmail, mPassword) ;
				
				 final String  METHOD_NAME ="register" ; 
				
				SoapObject request = new SoapObject(com.proximizer.client.Global.NAMESPACE, METHOD_NAME);
				 request.addProperty("arg0",mEmail);
		         request.addProperty("arg1",mPassword);
		         SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		         envelope.setOutputSoapObject(request);
		        
		         // Make the soap call.
		         HttpTransportSE androidHttpTransport = new HttpTransportSE(com.proximizer.client.Global.URL);
		         androidHttpTransport.debug=true ;
		         try {

		             //this is the actual part that will call the webservice
		             androidHttpTransport.call(com.proximizer.client.Global.SOAP_ACTION, envelope);        
		         } catch (Exception e) {
		             e.printStackTrace(); 
		         }
		         finally
		         {
		        	 String inputRequest =androidHttpTransport.requestDump ;
		             //Log.d(TAG,"Input XML request "+inputRequest);
		             
		             String outputResponse =androidHttpTransport.responseDump ;
		             //Log.d(TAG,"Output XML response "+outputResponse);
		         }
		         
		      // Get the SoapResult from the envelope body.       
		         SoapObject result = (SoapObject)envelope.bodyIn;
		         //Log.d("Soap response","result  is ....."+result);
		         
		         if (result !=null)
		         {
		        	 String authresult =result.getProperty(0).toString();
		        	 if (authresult.equals("true"))
		        	 {
		        			 isRegister=true;
		        			 
		        	 }
		         }
		         
				return isRegister ;
				
			} catch (Exception e) {
				return false;
			}

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				//finish();
			
				
				Toast.makeText(getApplicationContext(),"Registration successful", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(),"Registration Unsuccessful. Userid already exists", Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	
	
}
